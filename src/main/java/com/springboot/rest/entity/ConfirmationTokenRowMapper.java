package com.springboot.rest.entity;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.rest.service.UtilityService;

/**
 * SpringBootRestDemo 2019 Filename: ConfirmationTokenRowMapper.java Description: ConfirmationTokenRowMapper class
 * request and response to appropriate format,
 *
 * @author Itmusketeers
 * @version 1.0
 * @Last modified 2019-03-07
 */
public class ConfirmationTokenRowMapper implements RowMapper<ConfirmationTokenModel>{

	
	/**
	 * Method is used for map database result set to the model file.
	 *
	 * @author Itmusketeers
	 * @version 1.0
	 * @Last modified 2019-03-07
	 */
	@Override
	public ConfirmationTokenModel mapRow(ResultSet row, int rowNum) throws SQLException {
		
		ConfirmationTokenModel confirmationTokenModel = new ConfirmationTokenModel();
		confirmationTokenModel.setTokenid(new BigInteger(row.getString("confirmationtokenPk")));
		confirmationTokenModel.setConfirmationToken(row.getString("confirmationtoken"));
		confirmationTokenModel.setCreatedDate(UtilityService.convertStringTODate(row.getString("createdDate")));
		confirmationTokenModel.setUser(new BigInteger(row.getString("userid")));
		return confirmationTokenModel;
	}

}
