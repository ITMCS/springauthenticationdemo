package com.springboot.rest.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.springboot.rest.entity.UserModel;

@Service
public interface UserService {
	
	
	/* Used for get all User form database */
	List<UserModel> getAllUsers();
	
	/* Used for insert User form database */
	String addUser(UserModel user) throws Exception;
	
	/* Used for update User form database */
    void updateUser(UserModel user);
    
    /* Used for confirm the account aciton */
    String confrimAccountAction(String token);

    /* Used for authenticate the User form database */
    String authenticatUser(UserModel user);
}
