package com.springboot.rest.service;

import java.util.List;
import java.util.UUID;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.springboot.rest.dao.DBOperationInterface;
import com.springboot.rest.entity.ConfirmationTokenModel;
import com.springboot.rest.entity.UserModel;

/**
 * SpringBootRestDemo 2019 Filename: UserServiceImpl.java Description: UserServiceImpl
 * class request and response to appropriate format,
 *
 * @author Itmusketeers
 * @version 1.0
 * @Last modified 2019-03-07
 */
@Service
public class UserServiceImpl implements UserService{

	@Autowired 
	private DBOperationInterface dbOperations;
	
	@Autowired
	private Environment env;

	/**
	 *  Used for get all User form database 
	 *
	 * @author Itmusketeers
	 * @version 1.0
	 * @Last modified 2019-03-07
	 */
	@Override
	public List<UserModel> getAllUsers() {
		return dbOperations.getAllUsers();
	}

	/**
	 * Used for insert User form database
	 *
	 * @author Itmusketeers
	 * @version 1.0
	 * @Last modified 2019-03-07
	 */
	@Override
	public synchronized String addUser(UserModel user) throws Exception{
		if (dbOperations.userExists(user)) {
			String sqlStatus = "DUPLICATE";
			UserModel userModel = dbOperations.fetchUserbyUserName(user.getUserName());
			if (userModel.getIsVerify() == 0) {
				sqlStatus = "Already send link to the Registed email id " + userModel.getUserName();
			}
			return sqlStatus;
		} else {
			user.setPassword(EncryptDecryptStringWithDES.encrypt(user.getPassword(), user.getUserName())); 
			dbOperations.addUser(user);
			UserModel userModel = dbOperations.fetchUserbyUserName(user.getUserName());
			prepareEmailContent(userModel);
			return "SUCCESSINSERT";
		}
	}

	private void prepareEmailContent(UserModel userModel) throws Exception {
		
		ConfirmationTokenModel objConfirmationTokenModel = new ConfirmationTokenModel();
		objConfirmationTokenModel.setConfirmationToken(UUID.randomUUID().toString());
		objConfirmationTokenModel.setUser(userModel.getUserId());
		
		dbOperations.addConfirmationToken(objConfirmationTokenModel);
		String url = "http://"+env.getProperty("server.host")+":"+env.getProperty("server.port")+"/user/confirm-account?token="+objConfirmationTokenModel.getConfirmationToken();
		
		String confirmationtoken = "To confirm your account, please click here : "
	            +"<a href='"+url+"'>"+url+"</a>";
		UtilityService.sendMail(userModel.getUserName(),"Complete Registration!",confirmationtoken,env);
		
	}

	/**
	 * Used for update User form database
	 *
	 * @author Itmusketeers
	 * @version 1.0
	 * @Last modified 2019-03-07
	 */
	@Override
	public void updateUser(UserModel user) {
		dbOperations.updateUser(user);
	}

	/**
	 * Here update the user and delete the previous token from the database and also check 
	 * token is steel is exist or not first.
	 *
	 * @author Itmusketeers
	 * @version 1.0
	 * @Last modified 2019-03-07
	 */
	@Override
	public String confrimAccountAction(String token) {
		String resposne = "WRONGTOKEN";
		if(dbOperations.tokenExists(token)) {
			ConfirmationTokenModel objConfirmationTokenModel = dbOperations.fetchConfirmTokenModelbyToken(token);
			UserModel userModel = new UserModel();
			userModel.setIsVerify(1);
			userModel.setUserId(objConfirmationTokenModel.getUser());
			dbOperations.updateUser(userModel);
			dbOperations.deleteConfirmToken(objConfirmationTokenModel.getUser());
			resposne = "SUCCESSUPDATE";			
		}
		return resposne;
	}

	/**
	 * Method is used for authentication of user
	 *
	 * @author Itmusketeers
	 * @version 1.0
	 * @Last modified 2019-03-07
	 */
	@Override
	public String authenticatUser(UserModel user) {
		String resposne = "INVALIDUSER";
		if (dbOperations.userExists(user)) {
			UserModel userModel = dbOperations.fetchUserbyUserName(user.getUserName());
			if (userModel.getIsVerify() == 1) {
				String inputDetails = EncryptDecryptStringWithDES.decrypt(userModel.getPassword(),
						userModel.getUserName());
				resposne = inputDetails.equalsIgnoreCase(user.getPassword()) ? "SUCCESSAUTHENTICATION" : "INVALIDPASSWORD";
			} else {
				resposne = "REMAINEMAILAUTH";
			}
		}
		return resposne;
	}

}
