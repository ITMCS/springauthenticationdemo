package com.springboot.rest.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.rest.entity.UserModel;
import com.springboot.rest.service.ResponseGeneratorService;
import com.springboot.rest.service.UserService;

/**
 * SpringBootRestDemo 2019 Filename: UserController.java Description: Controller class
 * request and response to appropriate format,
 *
 * @author Itmusketeers
 * @version 1.0
 * @Last modified 2019-03-07
 */
@RestController
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;

	/**
     * Save Data and User model used as a parameter where some important details are required,
     * like  firstname, lastname, username, password.
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@PostMapping("save")
	public ResponseEntity<Void> addArticle(@RequestBody UserModel userModel) {
		try {
			String status = userService.addUser(userModel);
			if ("DUPLICATE".equalsIgnoreCase(status)) {

				return new ResponseEntity(ResponseGeneratorService.errorResponse(status, "DUPLICATEUSERNAME"),
						HttpStatus.CONFLICT);

			} else if ("SUCCESSINSERT".equalsIgnoreCase(status)) {
				return new ResponseEntity(ResponseGeneratorService.successResponse(
						"Please click on authentication link which sent on registered email " + userModel.getUserName(),
						status), HttpStatus.CREATED);
			} else {

				return new ResponseEntity(ResponseGeneratorService.errorResponse(status, "COMPLETEAUTHENTICATION"),
						HttpStatus.FORBIDDEN);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity(ResponseGeneratorService.errorResponse("Server error", e),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
     * Used for fetch all data about users
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@GetMapping("fetchAll")
	public ResponseEntity getAllUsers() {
		String resposneMsg = "Some Technical Error Occured";
		try {
			List<UserModel> list = userService.getAllUsers();
			resposneMsg = list.isEmpty() ? "There is no user exist" : "All Users Fetch Successfully";
			return new ResponseEntity(ResponseGeneratorService.successResponse(resposneMsg, list), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity(ResponseGeneratorService.errorResponse(resposneMsg, e),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
     * After Successfully sign up this method is used for authenticate the token and update the user
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@GetMapping("confirm-account")
	public ResponseEntity<Void> confirmAccount(HttpServletRequest request) {
		String resposneMsg = "Some Technical Error Occured";
		try {			
			String resposne = userService.confrimAccountAction(request.getParameter("token"));
			if("SUCCESSUPDATE".equalsIgnoreCase(resposne)) {
				return new ResponseEntity(ResponseGeneratorService.successResponse("You have successfully verified your email address",resposne ), HttpStatus.OK);
			}else {
				return new ResponseEntity(ResponseGeneratorService.successResponse("Your url is expired or wrong", resposne), HttpStatus.CONFLICT);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity(ResponseGeneratorService.errorResponse(resposneMsg, e),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
     * Login mehtod and User model used as a parameter where some important details are required,
     * like  username, password.
     * 
     * @author Itmusketeers
     * @version 1.0
     * @Last modified 2019-03-07
     */
	@PostMapping("login")
	public ResponseEntity<Void> checkLogin(@RequestBody UserModel userModel,HttpServletRequest request) {
		String resposneMsg = "Some Technical Error Occured";
		try {
			String resposne = userService.authenticatUser(userModel);
			if("SUCCESSAUTHENTICATION".equalsIgnoreCase(resposne)) {
				return new ResponseEntity(ResponseGeneratorService.successResponse("You are successfully login to your account",resposne), HttpStatus.OK);
			}else if("REMAINEMAILAUTH".equalsIgnoreCase(resposne)) {
				return new ResponseEntity(ResponseGeneratorService.successResponse("Please first complete your email verification",resposne), HttpStatus.DESTINATION_LOCKED);
			}else {
				return new ResponseEntity(ResponseGeneratorService.errorResponse("Your requested data is invalid, Please check errorCode ",resposne), HttpStatus.FORBIDDEN);
			}			
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity(ResponseGeneratorService.errorResponse(resposneMsg, e),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}

}
